\babel@toc {french}{}
\contentsline {paragraph}{}{1}{section*.1}%
\contentsline {paragraph}{}{1}{section*.2}%
\contentsline {paragraph}{}{1}{section*.3}%
\contentsline {paragraph}{}{3}{section*.5}%
\contentsline {paragraph}{}{3}{section*.6}%
\contentsline {section}{\numberline {1}Introduction}{4}{section.1}%
\contentsline {paragraph}{}{4}{section*.7}%
\contentsline {section}{\numberline {2}Manipulations XML}{5}{section.2}%
\contentsline {subsection}{\numberline {2.1}Général}{5}{subsection.2.1}%
\contentsline {paragraph}{}{5}{section*.8}%
\contentsline {paragraph}{}{5}{section*.9}%
\contentsline {paragraph}{}{5}{section*.10}%
\contentsline {paragraph}{}{5}{section*.11}%
\contentsline {paragraph}{}{6}{section*.12}%
\contentsline {paragraph}{}{6}{section*.13}%
\contentsline {subsection}{\numberline {2.2}Outils de développement}{6}{subsection.2.2}%
\contentsline {paragraph}{}{6}{section*.14}%
\contentsline {paragraph}{}{6}{section*.15}%
\contentsline {paragraph}{}{6}{section*.16}%
\contentsline {subsection}{\numberline {2.3}Fonctionnalités implémentées}{7}{subsection.2.3}%
\contentsline {paragraph}{}{7}{section*.17}%
\contentsline {paragraph}{}{8}{section*.18}%
\contentsline {paragraph}{}{8}{section*.19}%
\contentsline {paragraph}{}{9}{section*.20}%
\contentsline {subsection}{\numberline {2.4}Analyse critique}{9}{subsection.2.4}%
\contentsline {paragraph}{}{9}{section*.21}%
\contentsline {paragraph}{}{9}{section*.22}%
\contentsline {paragraph}{}{9}{section*.23}%
\contentsline {paragraph}{}{9}{section*.24}%
\contentsline {paragraph}{}{10}{section*.25}%
\contentsline {subsection}{\numberline {2.5}Reproductibilité}{10}{subsection.2.5}%
\contentsline {paragraph}{}{10}{section*.26}%
\contentsline {paragraph}{}{10}{section*.27}%
\contentsline {section}{\numberline {3}Annotations manuelles du corpus:}{11}{section.3}%
\contentsline {subsection}{\numberline {3.1}Introduction au travail d'annotateur:}{11}{subsection.3.1}%
\contentsline {paragraph}{}{11}{section*.28}%
\contentsline {paragraph}{}{11}{section*.29}%
\contentsline {paragraph}{}{11}{section*.30}%
\contentsline {paragraph}{}{11}{section*.31}%
\contentsline {subsection}{\numberline {3.2}Études des outils existant:}{11}{subsection.3.2}%
\contentsline {paragraph}{}{11}{section*.32}%
\contentsline {subsection}{\numberline {3.3}Annotation de la 26ème journée des minutes du procès de Nuremberg:}{12}{subsection.3.3}%
\contentsline {paragraph}{}{12}{section*.33}%
\contentsline {paragraph}{Consignes d'annotations:}{12}{section*.34}%
\contentsline {paragraph}{}{12}{section*.35}%
\contentsline {paragraph}{}{12}{section*.36}%
\contentsline {paragraph}{}{12}{section*.37}%
\contentsline {paragraph}{}{13}{section*.38}%
\contentsline {paragraph}{}{13}{section*.39}%
\contentsline {paragraph}{}{13}{section*.40}%
\contentsline {section}{\numberline {4}Mesures d'accord}{14}{section.4}%
\contentsline {paragraph}{}{14}{section*.41}%
\contentsline {subsection}{\numberline {4.1}État de l'art :}{14}{subsection.4.1}%
\contentsline {subsubsection}{\numberline {4.1.1}Mesure d'accord et correction par la chance:}{14}{subsubsection.4.1.1}%
\contentsline {paragraph}{}{14}{section*.42}%
\contentsline {paragraph}{}{14}{section*.43}%
\contentsline {subsubsection}{\numberline {4.1.2}Mesure d'accord adapté à "l'Unitizing":}{14}{subsubsection.4.1.2}%
\contentsline {paragraph}{}{14}{section*.44}%
\contentsline {paragraph}{}{15}{section*.45}%
\contentsline {paragraph}{}{15}{section*.46}%
\contentsline {subsection}{\numberline {4.2}Mesure de l'accord sur notre campagne d'annotation:}{15}{subsection.4.2}%
\contentsline {subsubsection}{\numberline {4.2.1}Accord première passe d'annotation.}{15}{subsubsection.4.2.1}%
\contentsline {paragraph}{}{15}{section*.47}%
\contentsline {paragraph}{}{15}{section*.48}%
\contentsline {paragraph}{Analyse des résultats : }{16}{section*.49}%
\contentsline {paragraph}{}{16}{section*.50}%
\contentsline {subsubsection}{\numberline {4.2.2}Accord seconde passe d'annotation.}{16}{subsubsection.4.2.2}%
\contentsline {paragraph}{}{16}{section*.51}%
\contentsline {paragraph}{}{16}{section*.52}%
\contentsline {paragraph}{Analyse des résultats : }{17}{section*.53}%
\contentsline {paragraph}{}{17}{section*.54}%
\contentsline {subsection}{\numberline {4.3}Travaux/expériences annexes réalisés sur la mesure d'accord :}{18}{subsection.4.3}%
\contentsline {paragraph}{}{18}{section*.55}%
\contentsline {subsubsection}{\numberline {4.3.1}Prise en compte des valeurs manquantes.}{18}{subsubsection.4.3.1}%
\contentsline {paragraph}{}{18}{section*.56}%
\contentsline {paragraph}{}{18}{section*.57}%
\contentsline {paragraph}{}{18}{section*.58}%
\contentsline {subsubsection}{\numberline {4.3.2}Visualisation et simulation de la chance pour l'unitizing.}{19}{subsubsection.4.3.2}%
\contentsline {paragraph}{}{19}{section*.59}%
\contentsline {paragraph}{}{19}{section*.60}%
\contentsline {paragraph}{}{19}{section*.61}%
\contentsline {paragraph}{}{19}{section*.62}%
\contentsline {paragraph}{}{19}{section*.63}%
\contentsline {paragraph}{}{20}{section*.64}%
\contentsline {paragraph}{Développement: }{20}{section*.65}%
\contentsline {paragraph}{}{21}{section*.66}%
\contentsline {paragraph}{}{21}{section*.67}%
\contentsline {paragraph}{}{22}{section*.68}%
\contentsline {paragraph}{}{22}{section*.69}%
\contentsline {paragraph}{Améliorations possibles de l'application}{22}{section*.70}%
\contentsline {section}{\numberline {5}Traitement automatique des langues - TAL}{23}{section.5}%
\contentsline {paragraph}{}{23}{section*.71}%
\contentsline {subsection}{\numberline {5.1}État de l'art}{23}{subsection.5.1}%
\contentsline {paragraph}{}{23}{section*.72}%
\contentsline {paragraph}{}{23}{section*.73}%
\contentsline {subsection}{\numberline {5.2}Expérimentation}{23}{subsection.5.2}%
\contentsline {paragraph}{}{23}{section*.74}%
\contentsline {paragraph}{}{23}{section*.75}%
\contentsline {paragraph}{}{23}{section*.76}%
\contentsline {paragraph}{}{23}{section*.77}%
\contentsline {paragraph}{}{24}{section*.78}%
\contentsline {paragraph}{}{24}{section*.79}%
\contentsline {paragraph}{}{24}{section*.80}%
\contentsline {paragraph}{}{24}{section*.81}%
\contentsline {paragraph}{}{24}{section*.82}%
\contentsline {paragraph}{}{24}{section*.83}%
\contentsline {paragraph}{}{25}{section*.84}%
\contentsline {paragraph}{}{25}{section*.85}%
\contentsline {paragraph}{}{25}{section*.86}%
\contentsline {paragraph}{}{25}{section*.87}%
\contentsline {paragraph}{}{25}{section*.88}%
\contentsline {paragraph}{}{25}{section*.89}%
\contentsline {paragraph}{}{25}{section*.90}%
\contentsline {paragraph}{}{25}{section*.91}%
\contentsline {paragraph}{}{25}{section*.92}%
\contentsline {paragraph}{}{25}{section*.93}%
\contentsline {paragraph}{}{25}{section*.94}%
\contentsline {paragraph}{}{25}{section*.95}%
\contentsline {paragraph}{}{25}{section*.96}%
\contentsline {paragraph}{}{26}{section*.97}%
\contentsline {paragraph}{}{26}{section*.98}%
\contentsline {paragraph}{}{26}{section*.99}%
\contentsline {paragraph}{}{26}{section*.100}%
\contentsline {paragraph}{}{27}{section*.101}%
\contentsline {paragraph}{}{27}{section*.102}%
\contentsline {paragraph}{}{27}{section*.103}%
\contentsline {section}{\numberline {6}Modélisation Sémantique de la Temporalité}{28}{section.6}%
\contentsline {paragraph}{}{29}{section*.104}%
\contentsline {paragraph}{}{29}{section*.105}%
\contentsline {paragraph}{}{29}{section*.106}%
\contentsline {paragraph}{Étape 1 - Création des axes:}{30}{section*.107}%
\contentsline {paragraph}{}{30}{section*.108}%
\contentsline {paragraph}{Étape 2 - Création des intervalles:}{30}{section*.109}%
\contentsline {paragraph}{}{30}{section*.110}%
\contentsline {paragraph}{}{30}{section*.111}%
\contentsline {paragraph}{Étape 3 - Règles sur les temps conjugués:}{30}{section*.112}%
\contentsline {paragraph}{}{30}{section*.113}%
\contentsline {paragraph}{Étape 4 - Règles inter-procès:}{31}{section*.114}%
\contentsline {paragraph}{}{31}{section*.115}%
\contentsline {paragraph}{Étape 5 - Règles pragmatiques:}{32}{section*.116}%
\contentsline {paragraph}{}{32}{section*.117}%
\contentsline {section}{\numberline {7}Mesure de validité}{34}{section.7}%
\contentsline {subsection}{\numberline {7.1}Mesurer la validité}{34}{subsection.7.1}%
\contentsline {paragraph}{}{34}{section*.118}%
\contentsline {subsection}{\numberline {7.2}Stratégies de mesure}{34}{subsection.7.2}%
\contentsline {paragraph}{}{34}{section*.119}%
\contentsline {subsection}{\numberline {7.3}Application et algorithmes}{34}{subsection.7.3}%
\contentsline {paragraph}{}{34}{section*.120}%
\contentsline {paragraph}{}{34}{section*.121}%
\contentsline {paragraph}{}{36}{figure.14}%
\contentsline {subsection}{\numberline {7.4}Présentation des résultats}{36}{subsection.7.4}%
\contentsline {subsubsection}{\numberline {7.4.1}Résultat de la version basique stricte}{36}{subsubsection.7.4.1}%
\contentsline {subsubsection}{\numberline {7.4.2}Résultat de la version avec tolérance (minimum la moitié de l'intersection}{36}{subsubsection.7.4.2}%
\contentsline {subsubsection}{\numberline {7.4.3}Résultat avec tolérance sur la catégorie}{37}{subsubsection.7.4.3}%
\contentsline {subsection}{\numberline {7.5}Analyse critique}{37}{subsection.7.5}%
\contentsline {paragraph}{}{37}{figure.15}%
\contentsline {paragraph}{}{37}{section*.124}%
\contentsline {paragraph}{}{37}{section*.125}%
\contentsline {paragraph}{}{38}{section*.126}%
\contentsline {paragraph}{}{38}{section*.127}%
\contentsline {paragraph}{}{38}{section*.128}%
\contentsline {section}{\numberline {8}Conclusion}{39}{section.8}%
\contentsline {paragraph}{}{39}{section*.129}%
